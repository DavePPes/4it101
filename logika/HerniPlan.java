package logika;


/**
 *  Class HerniPlan - třída představující mapu a stav adventury.
 * 
 *  Tato třída inicializuje prvky ze kterých se hra skládá:
 *  vytváří všechny prostory,
 *  propojuje je vzájemně pomocí východů 
 *  a pamatuje si aktuální prostor, ve kterém se hráč právě nachází.
 *
 *@author     Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova
 *@version    pro školní rok 2016/2017
 */
public class HerniPlan {
    
    private Prostor aktualniProstor;
    
    private Prostor vyherniProstor;
    
     /**
     *  Konstruktor který vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví halu.
     */
    public HerniPlan() {
        zalozProstoryHry();

    }
    /**
     *  Vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví domeček.
     */
    private void zalozProstoryHry() {
        // vytvářejí se jednotlivé prostory
        Prostor kraj = new Prostor("Kraj","Hobití vesnice, ze které Bilbo pochází.");
        Prostor roklinka = new Prostor("Roklinka","Elfské město");
        Prostor roklinkaZahrada = new Prostor("Zahrady","zahrady elfského město");
        Prostor roklinkaPalac = new Prostor("Palác","palác elfského město");
        Prostor troliTabor = new Prostor("Trolí_tábor","tábor zlých trolů");
        Prostor skretiJeskyne = new Prostor("Skřetí_jeskyně","Jeskyně plná skřetů");
        Prostor glumovaJeskyne = new Prostor("Glumova_jeskyně","Jeskyně, ve které se nachází glum");
        Prostor planina = new Prostor("Planina","Co tě zde asi čeká?");
        Prostor mlzneHory = new Prostor("Mlžné_hory","nebezpečné místo",true,"1","\n test textu",planina);
        Prostor temnyHvozd = new Prostor("Temný_hvozd","velmi nebezpečné místo");
        Prostor jezerniMesto = new Prostor("Jezerní_město","Město postavené na jezeře");
        Prostor osamelaHora = new Prostor("Osamělá_hora","Tvá cílová děstinace");
        Prostor osmelaHoraDraciMistnost = new Prostor("Dračí_místonst","Zde se nachází drak");
        Prostor osmelaHoraBonus = new Prostor("Bonusový_úkol","co si takhle vidělat");
        Prostor vytezneMisto = new Prostor("Vítězné_místo","Vyhrál jsi!");
        

        
        

        
        // přiřazují se průchody mezi prostory (sousedící prostory)
        kraj.setVychod(roklinka);
        roklinka.setVychod(kraj);
        roklinka.setVychod(roklinkaZahrada);
        roklinka.setVychod(roklinkaPalac);
        roklinka.setVychod(mlzneHory);
        roklinkaZahrada.setVychod(roklinka);
        roklinkaZahrada.setVychod(roklinkaPalac);
        roklinkaPalac.setVychod(roklinka);
        roklinkaPalac.setVychod(roklinkaZahrada);
        mlzneHory.setVychod(skretiJeskyne);
        mlzneHory.setVychod(troliTabor);
        troliTabor.setVychod(mlzneHory);
        skretiJeskyne.setVychod(glumovaJeskyne);
        skretiJeskyne.setVychod(planina);
        glumovaJeskyne.setVychod(planina);
        planina.setVychod(temnyHvozd);
        temnyHvozd.setVychod(jezerniMesto);
        jezerniMesto.setVychod(osamelaHora);
        osamelaHora.setVychod(osmelaHoraDraciMistnost);
        osmelaHoraDraciMistnost.setVychod(osmelaHoraBonus);
        osmelaHoraBonus.setVychod(vytezneMisto);
        
        
        
        
                
        
        Vec babovka = new Vec("Vodka",true);
        
        
        kraj.vlozVec(babovka);
       
                
        aktualniProstor = kraj;  // hra začíná v domečku  
        vyherniProstor = vytezneMisto;
    }
    
    /**
     *  Metoda vrací odkaz na aktuální prostor, ve ktetém se hráč právě nachází.
     *
     *@return     aktuální prostor
     */
    
    public Prostor getAktualniProstor() {
        return aktualniProstor;
    }
    
    /**
     *  Metoda nastaví aktuální prostor, používá se nejčastěji při přechodu mezi prostory
     *
     *@param  prostor nový aktuální prostor
     */
    public void setAktualniProstor(Prostor prostor) {
       aktualniProstor = prostor;
    }
    
    public boolean jeVyhra(){
        return aktualniProstor == vyherniProstor;
    }

}
