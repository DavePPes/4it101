/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logika;



/**
 *
 * @author Acer
 */
public class PrikazVolba implements IPrikaz{
    
    private static final String NAZEV = "volba";
    private HerniPlan plan;
    
    
    public PrikazVolba(HerniPlan plan) {
        this.plan = plan;
    }

    @Override
    public String provedPrikaz(String... parametry) {
         if (parametry.length == 0) {
            // pokud chybí druhé slovo (sousední prostor), tak ....
            return "Zadej co bude následovat, bez toho se v příběhu nehneš dále!";
        }
         
        
        Prostor aktualniProstor = plan.getAktualniProstor();
        
        String volba = parametry[0];
 
        if(volba.equals(aktualniProstor.getSpravnaVolba())){
            aktualniProstor.setVychod(aktualniProstor.getVychod());
            System.out.println("Gratuluji provedl jsi správnou volbu, níže můžeš vidět možné východy:");
            aktualniProstor.popisVychodu();
        }
        else{
            return "prohrál jsi";
        }
        return null;
        
        
        
    }
    
    
    
    public String getNazev(){
        return NAZEV;
    }
}
