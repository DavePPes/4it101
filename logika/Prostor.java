package logika;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Trida Prostor - popisuje jednotlivĂ© prostory (mĂ­stnosti) hry
 *
 * Tato tĹ™Ă­da je souÄŤĂˇstĂ­ jednoduchĂ© textovĂ© hry.
 *
 * "Prostor" reprezentuje jedno mĂ­sto (mĂ­stnost, prostor, ..) ve scĂ©nĂˇĹ™i hry.
 * Prostor mĹŻĹľe mĂ­t sousednĂ­ prostory pĹ™ipojenĂ© pĹ™es vĂ˝chody. Pro kaĹľdĂ˝ vĂ˝chod
 * si prostor uklĂˇdĂˇ odkaz na sousedĂ­cĂ­ prostor.
 *
 * @author Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova
 * @version pro ĹˇkolnĂ­ rok 2016/2017
 */
public class Prostor {

    private String nazev;
    private String popis;
    private boolean volba = false;
    private String spravnaVolba;
    private String textVolby;
    private Prostor vychod;
    private Set<Prostor> vychody;   // obsahuje sousednĂ­ mĂ­stnosti
    private Map<String,Vec> veci;

    /**
     * VytvoĹ™enĂ­ prostoru se zadanĂ˝m popisem, napĹ™. "kuchyĹ�", "hala", "trĂˇvnĂ­k
     * pĹ™ed domem"
     *
     * @param nazev nazev prostoru, jednoznaÄŤnĂ˝ identifikĂˇtor, jedno slovo nebo
     * vĂ­ceslovnĂ˝ nĂˇzev bez mezer.
     * @param popis Popis prostoru.
     */
    public Prostor(String nazev, String popis) {
        this.nazev = nazev;
        this.popis = popis;
        vychody = new HashSet<>();
        veci = new HashMap<>();
    }
    
    public Prostor(String nazev, String popis, boolean volba, String spravnaVolba, String textVolby, Prostor vychod) {
        this.nazev = nazev;
        this.popis = popis;
        vychody = new HashSet<>();
        veci = new HashMap<>();
        this.volba = volba;
        this.spravnaVolba = spravnaVolba;
        this.textVolby = textVolby;
        this.vychod = vychod;
    }

    /**
     * Definuje vĂ˝chod z prostoru (sousednĂ­/vedlejsi prostor). Vzhledem k tomu,
     * Ĺľe je pouĹľit Set pro uloĹľenĂ­ vĂ˝chodĹŻ, mĹŻĹľe bĂ˝t sousednĂ­ prostor uveden
     * pouze jednou (tj. nelze mĂ­t dvoje dveĹ™e do stejnĂ© sousednĂ­ mĂ­stnosti).
     * DruhĂ© zadĂˇnĂ­ stejnĂ©ho prostoru tiĹˇe pĹ™epĂ­Ĺˇe pĹ™edchozĂ­ zadĂˇnĂ­ (neobjevĂ­ se
     * ĹľĂˇdnĂ© chybovĂ© hlĂˇĹˇenĂ­). Lze zadat tĂ©Ĺľ cestu ze do sebe sama.
     *
     * @param vedlejsi prostor, kterĂ˝ sousedi s aktualnim prostorem.
     *
     */
    public void setVychod(Prostor vedlejsi) {
        vychody.add(vedlejsi);
    }

    /**
     * Metoda equals pro porovnĂˇnĂ­ dvou prostorĹŻ. PĹ™ekrĂ˝vĂˇ se metoda equals ze
     * tĹ™Ă­dy Object. Dva prostory jsou shodnĂ©, pokud majĂ­ stejnĂ˝ nĂˇzev. Tato
     * metoda je dĹŻleĹľitĂˇ z hlediska sprĂˇvnĂ©ho fungovĂˇnĂ­ seznamu vĂ˝chodĹŻ (Set).
     *
     * BliĹľĹˇĂ­ popis metody equals je u tĹ™Ă­dy Object.
     *
     * @param o object, kterĂ˝ se mĂˇ porovnĂˇvat s aktuĂˇlnĂ­m
     * @return hodnotu true, pokud mĂˇ zadanĂ˝ prostor stejnĂ˝ nĂˇzev, jinak false
     */  
      @Override
    public boolean equals(Object o) {
        // porovnĂˇvĂˇme zda se nejednĂˇ o dva odkazy na stejnou instanci
        if (this == o) {
            return true;
        }
        // porovnĂˇvĂˇme jakĂ©ho typu je parametr 
        if (!(o instanceof Prostor)) {
            return false;    // pokud parametr nenĂ­ typu Prostor, vrĂˇtĂ­me false
        }
        // pĹ™etypujeme parametr na typ Prostor 
        Prostor druhy = (Prostor) o;

        //metoda equals tĹ™Ă­dy java.util.Objects porovnĂˇ hodnoty obou nĂˇzvĹŻ. 
        //VrĂˇtĂ­ true pro stejnĂ© nĂˇzvy a i v pĹ™Ă­padÄ›, Ĺľe jsou oba nĂˇzvy null,
        //jinak vrĂˇtĂ­ false.

       return (java.util.Objects.equals(this.nazev, druhy.nazev));       
    }

    /**
     * metoda hashCode vraci ciselny identifikator instance, ktery se pouziva
     * pro optimalizaci ukladani v dynamickych datovych strukturach. Pri
     * prekryti metody equals je potreba prekryt i metodu hashCode. Podrobny
     * popis pravidel pro vytvareni metody hashCode je u metody hashCode ve
     * tride Object
     */
    @Override
    public int hashCode() {
        int vysledek = 3;
        int hashNazvu = java.util.Objects.hashCode(this.nazev);
        vysledek = 37 * vysledek + hashNazvu;
        return vysledek;
    }
      

    /**
     * VracĂ­ nĂˇzev prostoru (byl zadĂˇn pĹ™i vytvĂˇĹ™enĂ­ prostoru jako parametr
     * konstruktoru)
     *
     * @return nĂˇzev prostoru
     */
    public String getNazev() {
        return nazev;       
    }
    
    public Boolean getVolba(){
        return volba;
    }
    
    public String getSpravnaVolba(){
       if(this.spravnaVolba != null){
           return this.spravnaVolba;
       }
       else return "";
       
    }
    
    public Prostor getVychod(){
        return vychod;
    }

    /**
     * VracĂ­ "dlouhĂ˝" popis prostoru, kterĂ˝ mĹŻĹľe vypadat nĂˇsledovnÄ›: Jsi v
     * mistnosti/prostoru vstupni hala budovy VSE na Jiznim meste. vychody:
     * chodba bufet ucebna
     *
     * @return DlouhĂ˝ popis prostoru
     */
    public String dlouhyPopis() {
        return "Jsi v mistnosti/prostoru " + popis + ".\n"
                + popisVychodu() + "\n"
                + popisVeci() + popisVolby(volba);
        
         
    }

    /**
     * VracĂ­ textovĂ˝ Ĺ™etÄ›zec, kterĂ˝ popisuje sousednĂ­ vĂ˝chody, napĹ™Ă­klad:
     * "vychody: hala ".
     *
     * @return Popis vĂ˝chodĹŻ - nĂˇzvĹŻ sousednĂ­ch prostorĹŻ
     */
    public String popisVychodu() {
        String vracenyText = "východy:";
        for (Prostor sousedni : vychody) {
            vracenyText += " " + sousedni.getNazev();
        }
        return vracenyText;
    }
    
    private String popisVolby(boolean volba){
    if(volba){
    return textVolby;
    }else return "";
    }

    /**
     * VracĂ­ prostor, kterĂ˝ sousedĂ­ s aktuĂˇlnĂ­m prostorem a jehoĹľ nĂˇzev je zadĂˇn
     * jako parametr. Pokud prostor s udanĂ˝m jmĂ©nem nesousedĂ­ s aktuĂˇlnĂ­m
     * prostorem, vracĂ­ se hodnota null.
     *
     * @param nazevSouseda JmĂ©no sousednĂ­ho prostoru (vĂ˝chodu)
     * @return Prostor, kterĂ˝ se nachĂˇzĂ­ za pĹ™Ă­sluĹˇnĂ˝m vĂ˝chodem, nebo hodnota
     * null, pokud prostor zadanĂ©ho jmĂ©na nenĂ­ sousedem.
     */
    public Prostor vratSousedniProstor(String nazevSouseda) {
        List<Prostor>hledaneProstory = 
            vychody.stream()
                   .filter(sousedni -> sousedni.getNazev().equals(nazevSouseda))
                   .collect(Collectors.toList());
        if(hledaneProstory.isEmpty()){
            return null;
        }
        else {
            return hledaneProstory.get(0);
        }
    }

    /**
     * VracĂ­ kolekci obsahujĂ­cĂ­ prostory, se kterĂ˝mi tento prostor sousedĂ­.
     * Takto zĂ­skanĂ˝ seznam sousednĂ­ch prostor nelze upravovat (pĹ™idĂˇvat,
     * odebĂ­rat vĂ˝chody) protoĹľe z hlediska sprĂˇvnĂ©ho nĂˇvrhu je to plnÄ›
     * zĂˇleĹľitostĂ­ tĹ™Ă­dy Prostor.
     *
     * @return NemodifikovatelnĂˇ kolekce prostorĹŻ (vĂ˝chodĹŻ), se kterĂ˝mi tento
     * prostor sousedĂ­.
     */
    public Collection<Prostor> getVychody() {
        return Collections.unmodifiableCollection(vychody);
    }
    
    public void vlozVec(Vec neco){
        veci.put(neco.getNazev(),neco);
    }
    
    private String popisVeci(){
        String vracenyText = "věci: ";
        for(String nazev : veci.keySet()){
            vracenyText += " ," + nazev;
        }
        return vracenyText;
    }
    
    public Vec odeberVec(String nazev){
        return veci.remove(nazev);
    }
}
